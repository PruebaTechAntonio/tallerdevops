var clientesObtenidos;

function getClientes() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function ()
  {
    if (this.readyState == 4 && this.status == 200){
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes()
{
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla = document.getElementById("tablaClientes");
  // var tabla = document.createElement("table");
  // var tabla_div = document.getElementById("tablaClientes");
  // tabla_div.appendChild(tabla);


  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;
    var columnaTelefono = document.createElement("td");
    columnaTelefono.innerText = JSONClientes.value[i].Phone;
    var columnaPais = document.createElement("td");
    columnaPais.innerText = JSONClientes.value[i].Country;
    var urlcolumnaBanderaInit = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    var urlcolumnaBandera = urlcolumnaBanderaInit + columnaPais.innerText + ".png";
    var columnaBandera = document.createElement("img");
    columnaBandera.classList.add("flag");
    if (JSONClientes.value[i].Country == "UK"){
      columnaBandera.src = urlcolumnaBanderaInit + "United-Kingdom.png";
    }
    else{
      columnaBandera.src = urlcolumnaBandera;
    }



    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaTelefono);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(columnaBandera);
    tabla.appendChild(nuevaFila);


    //alert(JSONProductos.value[i].ProductName);
  }
  //alert(JSONProductos.value[0].ProductName);
}
